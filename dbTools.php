<?php
    class DBopen extends SQLite3 {
        function __construct($thisFile) {
            $thisExists = file_exists($thisFile);
            $this->open($thisFile);
            if (!$thisExists) {
                // If database didn't exist, initialize it with tables for admin, team members and donors
                $ret = $this->exec("
                    CREATE TABLE admin (
                        adminName           VARCHAR(25) PRIMARY KEY NOT NULL,
                        date                CHAR(25)
                    );
                    INSERT INTO admin (adminName, date) VALUES ('memberFileDate', '" . (new DateTime('1970-01-02'))->format('c') . "');
                    INSERT INTO admin (adminName, date) VALUES ('memberDbDate',   '" . (new DateTime('1970-01-01'))->format('c') . "');
                    CREATE TABLE teammembers (
                        id                  CHAR(32) PRIMARY KEY    NOT NULL,
                        name                VARCHAR(99)             NOT NULL,
                        mail_address        VARCHAR(99)             NOT NULL,
                        password            VARCHAR(99)             NOT NULL,
                        teamrole            INT                     NOT NULL,
                        mycollectionaddr    VARCHAR(300)            NOT NULL,
                        fee                 INT                     NOT NULL
                    );
                    CREATE TABLE donors (
                        id                  CHAR(32) PRIMARY KEY    NOT NULL,
                        name                VARCHAR(99)             NOT NULL,
                        mail_address        VARCHAR(99),
                        phone               VARCHAR(99),
                        password            VARCHAR(99),
                        member_fee          INT
                    );"
                );
                if (!$ret) {
                    echo $this->lastErrorMsg();
                } else {
                    // echo "Table created successfully<br>\n";
                }
            }
        }

        // Generic SQL executor
        function execSQL($sql) {
            $ret = $this->exec($sql);
            if (!$ret) {
                echo $this->lastErrorMsg();
            }

            return $ret;
        }

        // This function checks if a table exists in the database
        function tableExists($table) {
            $ret = $this->query("SELECT name FROM sqlite_master WHERE type='table' AND name = '$table';");

            return $ret->fetchArray();
        }

        // This function inserts a team member into the database unless it already exists.
        // An update can be forced
        function insertTeamMember($id, $name, $mailAddr, $pwd, $roleInTeam, $myCollectionURL, $fee, $forceUpdate = false) {
            $ret = $this->getTeamMember($id);
            if (!$ret->fetchArray()) {
                $this->execSQL(
                    "INSERT INTO teammembers VALUES ('$id', '$name', '$mailAddr', '$pwd', $roleInTeam, '$myCollectionURL', $fee)"
                );
            }
            else if ($forceUpdate) {
                $this->execSQL(
                    "UPDATE teammembers SET (name = '$name', mail_address = '$mailAddr', teamrole = $roleInTeam, mycollectionaddr = '$myCollectionURL')
                    WHERE
                        id = '$id'"
                );
            }
        }

        function updateTeamMemberPassword($id, $pwd) {
            $this->execSQL(
                "UPDATE teammembers SET (password = '$pwd')
                WHERE
                    id = '$id'"
            );
        }
        function updateTeamMemberFee($id, $fee) {
            $this->execSQL(
                "UPDATE teammembers SET (fee = '$fee')
                WHERE
                    id = '$id'"
            );
        }

        function getTeamMember($id) {
            $ret = $this->query(
                "SELECT * FROM teammembers
                WHERE
                    id = '$id'"
            );

            return $ret;
        }

        function getDonor($id) {
            $ret = $this->query(
                "SELECT * FROM donors
                WHERE
                    id = '$id'"
            );

            return $ret;
        }

        function getAllDonors() {
            $ret = $this->query(
                "SELECT * FROM donors"
            );

            return $ret;
        }

        // This function inserts a donor into the database unless it already exists.
        function insertDonor($id, $name, $mailAddr, $phone = '', $pwd = '', $fee = 0) {
            $ret = $this->getDonor($id);
            if (!$ret->fetchArray()) {
                $this->execSQL(
                    "INSERT INTO donors VALUES ('$id', '$name', '$mailAddr', '$phone', '$pwd', $fee)"
                );
            }
        }

        // This function creates a table that shall contain booked legs for one team member. The team member's unique ID is part of the table name.
        function createBookedLegsTable($teamMemberId) {
            $this->execSQL(
                "
                    CREATE TABLE IF NOT EXISTS booked_legs_$teamMemberId (
                        leg_id       VARCHAR(12) PRIMARY KEY NOT NULL,
                        donor_id     CHAR(32)                NOT NULL,
                        pay_status   INT                     NOT NULL,
                        booking_date CHAR(25),
                        confirm_date CHAR(25)
                    )
                "
            );
        }

        // This function inserts a booked leg into one team member's booked legs table.
        // $confirmDate is in most cases not defined when the leg is created in the table. Therefore it's optional.
        function insertLeg($teamMemberId, $legId, $donorId, $payStatus, $bookDate, $confirmDate = '') {
            $table = "booked_legs_$teamMemberId";
            if (!$this->tableExists($table)) {
                $this->createBookedLegsTable($teamMemberId);
            }

            $this->execSQL(
                "INSERT INTO $table
                VALUES ('$legId', '$donorId', '$payStatus', '$bookDate', '$confirmDate');"
            );
        }

        // This function updates the confirmed booking date for a leg
        function insertLegConfirmedDate($teamMemberId, $legId, $confirmDate) {
            // TO DO:
            // Check if leg exists prior to update it
            $this->execSQL(
                sprintf("
                    UPDATE booked_legs_%s
                    SET
                        confirm_date = '%s',
                        pay_status   = '%s'
                    WHERE
                        leg_id = '%s';",
                    $teamMemberId, $confirmDate, payedConfirmed, $legId)
            );
        }

        function getBookedLegs($teamMemberId) {
            // Does the table exist?
            $table = "booked_legs_$teamMemberId";
            $ret = $this->tableExists($table);

            if ($ret) {
                // The table exists. Now, check if the leg is free.
                $ret = $this->query("SELECT * FROM $table");
            }

            return $ret;
        }

        // This function checks if any of the provided legs are booked already.
        // If table doesn't exist no legs are booked, which is obvious :)
        // The return value will either be 'false' or one or more rows matching the provided legs.
        function checkBookedLegs($teamMemberId, $legIds) {
            // Does the table exist?
            $table = "booked_legs_$teamMemberId";
            $ret = $this->tableExists($table);

            if ($ret) {
                // The table exists. Now, check if the leg is free.
                $ret = $this->query("SELECT * FROM $table
                                     WHERE
                                        leg_id in ($legIds)");
            }

            return $ret;
        }
    }

    function dbOpen($dbName) {
        $dB = new DBopen($dbName);
        if (!$dB) {
            echo $this->lastErrorMsg();
            throw new Exception("Could not open database $thisFile");
        }

        return $dB;
    }


?>
