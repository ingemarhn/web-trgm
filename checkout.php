<?php

header('Content-type: text/html; charset=UTF-8');
session_start();

require_once "variables.php";
$currentUser = isset($_SESSION['teamMemberName']) ? $_SESSION['teamMemberName'] : 'Not Set';
$lang = $_SESSION['lang'];
echo '<!DOCTYPE html>' . PHP_EOL;
echo "<html lang='$lang' >" . PHP_EOL;
echo '<head>';

# Name of this script
$scriptName = basename($_SERVER['SCRIPT_NAME'], '.php');

// Pick up the session variables we want to use
// If not correctly set, send user back to start page.
if (!isset($_SESSION['teamMemberId'])) {
    header('location: index.php');
    die(); // In case the browser/robot doesn't listen to the location directive.
}
$teamMemberId = $_SESSION['teamMemberId'];

error_reporting(E_ALL);

require_once "strings_$lang.php";
require_once 'stdhead.php';

// Open the database
require_once 'dbTools.php';
$db = dbOpen($dbName);

?>

</head>

<body>

    <div class="container">

        <?php

        require_once 'topHeader.php';

        // Get all selected leg parts from the bookings page
        $keys = '';
        foreach($_POST as $key => $val) {
            if (substr($key, 0, 3) == 'fld') {
                // Form a string to be used in the SQL query
                $keys .= ',' . "'$key'";
            }
        };
        $keys = substr($keys, 1);
        $_SESSION['keys'] = $keys;
        // Check if any of the legs are booked already
        $res = $db->checkBookedLegs($teamMemberId, $keys);
        if (!$res->fetchArray()) {
            // All selected legs are free to book
            // Add headers to HTML output
            $teamie = $_SESSION['teamMemberName'];
            echo <<<EOT
                <div class="hdr2">$youHaveSelected $teamie</div>
                <div class="checkoutTable">
                    <div class="tableHdr">$dateText</div>
                    <div class="tableHdr">$legText</div>
                    <div class="tableHdr">$distanceText</div>

EOT;

            $_SESSION['bookings'] = [];
            $n = 0;
            foreach($_POST as $key => $val) {
                if (substr($key, 0, 3) == 'fld') {
                    // Save the key in a session variable that can be accessed when the booked legs shall registered in the DB
                    $_SESSION['bookings'][$n++] = $key;
                    // Split the key to get selected row and column
                    $legItems = explode('_', $key);
                    $dat = $_SESSION["date_" . $legItems[1]];
                    // Remove the line break in the text description of the leg. Line break not needed.
                    $leg = str_replace('<br>', ' ', $_SESSION["leg_" . $legItems[1]]);
                    $km  = $_SESSION["km_" . $key];
                    $kms = $km . ' - ' . ($km + 10);
                    echo <<<EOT
                        <div class="tableElem">$dat</div>
                        <div class="tableElem">$leg</div>
                        <div class="tableElem">$kms</div>

EOT;
                }
            }
            echo '</div>' . PHP_EOL;
            $amountStr = $_POST['amountStr'];
            echo "<p></p><div class='hdr2'>$amountToDonate $amountStr</div>" . PHP_EOL;

            echo <<<EOT
                <div class="mainArea">
                    <form action="saveToDbAndShowMyCollection.php" method="POST" id="confirmForm">
                        <button type="submit" id="confirmButton">$confirmAndPay</button>
                    </form>
                    <div class="hdr3">$payingInfo</div>
                </div>
EOT;
        }
        else {
            echo '<div class="banner">' . $notFastEnough . '</div>';
        }

        ?>
    </div>

</body>
</html>
