<?php
    function redirectTo($URL, $waitTime) {

        echo <<<EOT
            <script>
            $(document).ready(function () {
                // Handler for .ready() called.
                window.setTimeout(
                    function () {
                        location.href = "$URL";
                    },
                    $waitTime
                );
            });
            </script>
EOT;

    }
?>
