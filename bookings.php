<?php

header('Content-type: text/html; charset=UTF-8');
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

session_start();

require_once "variables.php";
$currentUser = isset($_SESSION['teamMemberName']) ? $_SESSION['teamMemberName'] : 'Not Set';
$lang = $_SESSION['lang'];
echo '<!DOCTYPE html>' . PHP_EOL;
echo "<html lang='$lang' >" . PHP_EOL;
echo '<head>';

// Name of this script
$scriptName = basename($_SERVER['SCRIPT_NAME'], '.php');

// Save user mail address (it's mandatory, so, if we get here it is set on the previous page)
$_SESSION['userMailAddress'] = trim($_POST['userMailAddress']);
$_SESSION['userRealName']    = trim($_POST['userRealName']);

// The below variable definition is needed for the strings settings for this page in the variables file.
$userRole = $_POST['userRole'];
// Save the user role in the session, to be used on other pages.
$_SESSION['userRole'] = $userRole;

require_once "strings_$lang.php";
require_once "stdhead.php";

// Save the current team member in the session variable. Needed regardless of user role.
list($_SESSION['teamMemberName'], $_SESSION['teamMemberEmail']) = explode('|', $_POST['member']);
// Create the team member ID based on name and mail
$_SESSION['teamMemberId'] = md5($_SESSION['teamMemberName'] . $_SESSION['teamMemberEmail']);
// Who is the donor?
$_SESSION['donorId'] = md5(strtolower($_SESSION['userMailAddress']) . $_SESSION['userRealName']);
//echo $_SESSION['donorId'] . '<br>';
/*
When and if the time comes for introducing passwords, use these items
    $pwdHash = password_hash("rasmuslerdorf", PASSWORD_BCRYPT); // Mainly because the result is always 60 characters long
    password_verify("rasmuslerdorf", $pwdHash);
*/

error_reporting(E_ALL);

if (!isset($_SESSION['teamMemberId'])) {
    // Perhaps a restarted browser or a bookmark to this page
    redirectTo('.', 0);
}

// Read dates and distances, ...
require_once "readData.php";

// Open the database
require_once 'dbTools.php';
$db = dbOpen($dbName);
// Create the booked legs table for this team member. The function checks if the table exists or not before creation.
$db->createBookedLegsTable($_SESSION['teamMemberId']);

// Insert user/donor into database unless the ID already exists
if ($userRole == $roleDonor) {
    $db->insertDonor($_SESSION['donorId'], $_SESSION['userRealName'], $_SESSION['userMailAddress']);
}

$teamMemberFee = isset($_SESSION['teamMemberFee']) ? $_SESSION['teamMemberFee'] : $defaultFee;
// Global javascript variables set by PHP variable values
echo <<<EOT
<script>
    var donateText         = '$donorButtonFreeText';
    var bookedText         = '$donorButtonBookedText';
    var currency           = '$currency';
    var defaultFee         = $defaultFee;
    var teamMemberFee      = $teamMemberFee;
</script>

EOT;

?>

<script>
/**
 * This script handles the bookings.
 * - Modifies the buttons when they are clicked
 * - Keeps track of clicked buttons
 * - Loads the check out page and sends correct info to it
 */
    var nBookingsInSession = 0;
    var buttonUnBookedCls = 'donorBookingButton';
    //var buttonBookedCls   = 'donorBookedFieldPrelByMe';
    var buttonBookedCls   = 'donorBookedFieldByMe';

    $(document).ready(function() {

        $('.cartCheckoutButton').click(function() {
            var url;

            // Prevent the link in the cart to be followed if no segment have been booked
            if (nBookingsInSession == 0) {
                return false;
            }

            return true;
        });

        $('.donorBookingButton').click(function() {
            var getCartAmountStr = function(step) {
                nBookingsInSession += step;
                var amount = nBookingsInSession * teamMemberFee;
                var cartAmount = `${amount} ${currency}`;

                return cartAmount;
            }
            var currClass = $(this).attr('class');
            var buttonName  = $(this).attr('name');
            var input;

            // The hidden input field in the table must be the element previous to the current button.
            input = $(this).prev();
            var amountStr;
            if (currClass == buttonUnBookedCls) {
                var amountStr = getCartAmountStr(1);
                $('.cartQuantity').text(amountStr);
                if (nBookingsInSession == 1) {
                    $('.cartCheckoutButton').removeClass('disabled');
                    $('.cartCheckoutButton').addClass('enabled');
                }
                //bookedFieldsInSession[buttonName] = 1;
                $(this).removeClass(buttonUnBookedCls);
                $(this).addClass(buttonBookedCls);
                $(this).text(bookedText);
                // Add the name attribute to the corresponding input field, then the element will be forwarded in the POST request.
                input.attr('name', buttonName);
            }
            else if (currClass == buttonBookedCls) {
                var amountStr = getCartAmountStr(-1);
                $('.cartQuantity').text(amountStr);
                if (nBookingsInSession == 0) {
                    $('.cartCheckoutButton').removeClass('enabled');
                    $('.cartCheckoutButton').addClass('disabled');
                }
                //delete bookedFieldsInSession[buttonName];
                $(this).removeClass(buttonBookedCls);
                $(this).addClass(buttonUnBookedCls);
                $(this).text(donateText);
                // Remove the name attribute to the corresponding input field, so that the element won't be forwarded in the POST request
                input.removeAttr("name");
            }
            // Save the amountStr in the form hidden field
            $('input[name="amountStr"]').val(amountStr);
        });
    });
</script>

</head>

<body>

    <div class="container">

        <?php
            require_once 'topHeader.php';

            // Read already booked legs from database
            $booked = $db->getBookedLegs($_SESSION['teamMemberId']);
            // Save legs info in an array to be used to colorize the booking buttons further down in this file
            $legs     = [];
            $payStats = [];
            $donorIDs = [];
            while ($row = $booked->fetchArray()) {
                $legs[]     = $row['leg_id'];
                $payStats[] = $row['pay_status'];
                $donorIDs[] = $row['donor_id'];
            }
        ?>

        <!-- Shopping cart -->
        <div class="sticky-top-right">
            <!-- Place the submit button for the bookings form outside the form. Connect to the form with the "form" attribute. -->
            <button type="submit" form="bookings" class="cartCheckoutButton disabled">
                <div class="cartContainer">
                    <div class="cartImage">
                        <img src="images/cart.gif">
                    </div>
                    <?php
                        echo '<div class="cartQuantity">0 ' . $currency . '</div>' . PHP_EOL;
                        echo '<div class="cartCheckout">' . $goToCheckout . '</div>' . PHP_EOL;
                    ?>
                </div>
            </button>
        </div>

        <div class="hdr2">
            <?php
                echo $donateTo . '<span class="largerNcolored">' . $_SESSION['teamMemberName'] . '</span> - ' . $teamMemberFee . ' ' . $currency . '/10 ' . $distanceText;
            ?>
        </div>

        <div class="mainArea">
            <form action="checkout.php" method="POST" id="bookings">
                <div id="mainTable">
                    <div class="tblRow">
                        <?php
                            $nCol = 0;
                            $rowsPerColumn = [];
                            $maxRow = 0;
                            $th2 = '';
                            // Create the header in the bookings table
                            foreach ($distances as $date => $routeData) {
                                // Create the row with dates (output "now")
                                echo "<div class='tblHdr'>$date</div>" . PHP_EOL;
                                [$route, $kilometers] = $routeData;
                                // Save all dates and legs in the _SESSION variable. To be used in checkout.php to list selected legs.
                                $_SESSION["date_$nCol"] = $date;
                                $_SESSION["leg_$nCol"] = $route;
                                // Create the second row with the legs (build a string to output after the loop)
                                $th2 .= "<div class='tblHdr'>$route</div>" . PHP_EOL;
                                // Always round up to next integer
                                $rowsPerColumn[] = ceil($kilometers / 10);
                                $i = count($rowsPerColumn) - 1;
                                if ($i > $maxRow) {
                                    $maxRow = $rowsPerColumn[$i];
                                }
                                $nCol++;
                            }
                        ?>
                    </div>
                    <div class="tblRow">
                        <?php
                            echo $th2;
                            $nColumns = count($rowsPerColumn);
                        ?>
                    </div>
                    <?php
                        // Use the button as a visible element for the user to click. When the button is clicked a registered javascript function will store a value in the hidden input field.
                        // The hidden input field values are then sent on to the checkout page (it's not possible to send the button values).
                        $fmt = '<div class="tblDta"><span class="%s">%d</span> -<span class="%s">%d</span> <input type="hidden" id=%s> <button type="button" class="%s" name="%s">%s</span></div>' . PHP_EOL;
                        $clDist = 'distance';
                        for ($r = 0; $r < $maxRow; $r++) {
                            echo '<div class="tblRow">';
                            for ($c = 0; $c < $nColumns; $c++) {
                                if ($r < $rowsPerColumn[$c]) {
                                    $km = $r * 10;
                                    $nameButton = 'fld_' . $c . '_' . $r;
                                    // Check if current leg is booked or free
                                    $key = array_search($nameButton, $legs);
                                    if ($key === false) {
                                        $buttonCls = 'donorBookingButton';
                                        $buttonText = $donorButtonFreeText;
                                    }
                                    else {
                                        $buttonText = $donorButtonBookedText;
                                        if ($payStats[$key] == $payedUnconfirmed) {
                                            if ($donorIDs[$key] == $_SESSION['donorId']) {
                                                //$buttonCls = 'donorBookedFieldPrelByMe';
                                                $buttonCls = 'donorBookedFieldByMe';
                                            }
                                            else {
                                                //$buttonCls = 'donorBookedFieldPrelByOther';
                                                $buttonCls = 'donorBookedFieldByOther';
                                            }
                                        }
                                        else {
                                            // Pay status should be payedConfirmed. Assume that the state is correct :)
                                            if ($donorIDs[$key] == $_SESSION['donorId']) {
                                                $buttonCls = 'donorBookedFieldByMe';
                                            }
                                            else {
                                                $buttonCls = 'donorBookedFieldByOther';
                                            }
                                        }
                                    }
//echo $donorIDs[$key] . ': ';
                                    printf($fmt, $clDist, $km, $clDist, $km + 10, $nameButton, $buttonCls, $nameButton, $buttonText);
                                    // Save all part distances to be able to be listed in checkout.php
                                    $_SESSION["km_$nameButton"] = $km;
                                }
                                else {
                                    echo '<div class="tblDta"></div>' . PHP_EOL;
                                }
                            }
                            echo '</div>' . PHP_EOL;
                        }
                    ?>
                </div>
                <input type="hidden" name="amountStr" value="">
            </form>
            <br>
            <?php
            echo <<<EOT
                <span class="legend">
                    $legend:
                    <button class="donorBookingButton">$donorButtonFreeText</button>&nbsp;$free&nbsp;&nbsp;&nbsp;
                    <!-- <button class="donorBookedFieldPrelByMe">$donorButtonBookedText</button>&nbsp;\$bookedByYouPrel&nbsp;&nbsp;&nbsp; -->
                    <button class="donorBookedFieldByMe">$donorButtonBookedText</button>&nbsp;$bookedByYou&nbsp;&nbsp;&nbsp;
                    <!-- <button class="donorBookedFieldPrelByOther">$donorButtonBookedText</button>&nbsp;\$bookedByOtherPrel&nbsp;&nbsp;&nbsp; -->
                    <button class="donorBookedFieldByOther">$donorButtonBookedText</button>&nbsp;$bookedByOther&nbsp;&nbsp;&nbsp;
                </span>
EOT;
            ?>
        </div>
    </div>

</body>
</html>
