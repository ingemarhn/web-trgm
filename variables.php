<?php

    // This file defines some global variables, except strings for the selected language, which are located in separate files.

    // Users role
    $roleRider      = 1;
    $roleService    = 2;
    $roleDonor      = 3;
    $roleTeamMember = 4;

    // Database name
    $dbName = 'TRGM.sqlite';

    // Mail address to use as sender for mails informing about new bookings
    // It's very likely that each team member have to add this address as accepted
    // mail address to avoid that it ends up in the spam folder
    $senderMailAddress = 'noreply@spinnare.eu';

    // Used distance type
    $distanceKm  = 101;
    $distanceMil = 102;

    // Default fee. Set a value in the local currency, e.g. 100 SEK. Last day can have a different value.
    // Use this fee if team member doesn't specify anything else
    $defaultFee        = 100;
    //$defaultFeeLastDay = 200; -- Not implemented

    // Currency and format when an amount of money is presented
    $currency = 'SEK';
    // -- not implemented   $amountFormat = '%d %s'; // sprintf format string. Limitation: Amount and currency to be presented in any order and with or without space between. No other values possble without modifying the scripts.

    // Payment status
    $payedUnconfirmed = 201;
    $payedConfirmed   = 202;

    // Region settings
    $defaultLang = 'sv-SE';
    // Use timezone_identifiers_list() to list all available time zones
    date_default_timezone_set('Europe/Stockholm');

?>
