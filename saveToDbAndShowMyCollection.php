<?php

header('Content-type: text/html; charset=UTF-8');
session_start();

require_once "variables.php";
$lang = $_SESSION['lang'];
echo '<!DOCTYPE html>' . PHP_EOL;
echo "<html lang='$lang' >" . PHP_EOL;
echo '<head>';

# Name of this script
$scriptName = basename($_SERVER['SCRIPT_NAME'], '.php');

// Pick up the session variables we want to use
// If not correctly set, send user back to start page.
if (!isset($_SESSION['teamMemberId'])) {
    header('location: index.php');
    die(); // In case the browser/robot doesn't listen to the location directive.
}
$teamMemberId = $_SESSION['teamMemberId'];

error_reporting(E_ALL);

require_once "strings_$lang.php";
require_once "stdhead.php";

// Open the database
require_once 'dbTools.php';
$db = dbOpen($dbName);

require_once 'phpTools.php';

?>

</head>

<body>

    <div class="container">
        <?php

        require_once 'topHeader.php';

        // A last check to see if someone else has booked any of these legs
        $res = $db->checkBookedLegs($teamMemberId, $_SESSION['keys']);
        if (!$res->fetchArray()) {
            $res = $db->getTeamMember($teamMemberId);
            if ($row = $res->fetchArray()) {
                $myCollection = $row['mycollectionaddr'];
            }

            echo '<div class="banner">' . $redirectMessage . '</div>';
            echo '<br><a href="' . $myCollection . '">' . $backupLink . '</a>';

            $dateTime = date('c');
            foreach ($_SESSION['bookings'] as $field) {
                $db->insertLeg($teamMemberId, $field, $_SESSION['donorId'], $payedUnconfirmed, $dateTime);
            }

            // Send mail to team member about bookings
            $to = $_SESSION['teamMemberEmail'];
            $subject = $_SESSION['userRealName'] . $prelBooking;
            $msg = '';
            foreach ($_SESSION['bookings'] as $field) {
                $legItems = explode('_', $field);
                // Remove a possible line break in the leg
                $leg = str_replace('<br>', ' ', $_SESSION["leg_" . $legItems[1]]);
                // Remove all other possible HTML tags in the leg
                $leg = preg_replace('/<.*?>/', '', $leg);
                $km  = $_SESSION["km_" . $field];
                $kms = $km . ' - ' . ($km + 10);
                $msg .= $leg . '' . ": $kms $distanceText\n";
            }

            $headers = [
                'From' => $senderMailAddress,
                'Reply-To' => $senderMailAddress,
                'X-Mailer' => 'PHP/' . phpversion()
            ];

            mail($to, $subject, $msg, $headers);
            unset($_SESSION['bookings']);

            // Send mail to supporter about what he/she have booked
            $to = $_SESSION['userMailAddress'];
            $subject = $you . ' ' . $prelBooking . ' ' . $for . $_SESSION['teamMemberName'];
            $msg .= sprintf($thanks, $_SESSION['teamMemberName']);
            mail($to, $subject, $msg, $headers);

            redirectTo($myCollection, 5000);
        }
        else {
            echo '<div class="banner">' . $notFastEnough . '</div>';
        }

        ?>
    </div>
</body>
</html>
