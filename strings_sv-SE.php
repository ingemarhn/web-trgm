<?php

    # This file defines some global strings. Only define strings here that shall be translated or otherwise modified by a team.
    # The script may require that some variables are set. See code below for which.

    switch ($scriptName) {
        case 'index':
            // Common string variables for all pages
            $pageTitle = "Donera för mina mil - Team Rynkeby - God Morgon Lund";
            $topHeader = 'Om du donerar så cyklar vi!';

            // Page specific strings
            $initText         = 'Nja, alltså, vi cyklar ju ändå. Men det kommer att gå så mycket lättare om vi vet att någon har donerat pengar för den väg vi cyklar.';
            $projectText      = 'Har du kommit så här långt så känner du förmodligen till en massa om projektet redan. Men vill du veta mer så klicka på loggan till höger.';
            $collectionText   = 'Tanken med denna insamling är förstås att få in ännu mer pengar till <a href="https://www.barncancerfonden.se/" target="_blank">Barncancerfonden</a> för att hjälpa de sjuka barnen. ' .
                                'Vi hoppas att det ska kännas lite lockande att välja ut en eller flera sträckor där du kan skänka pengar. ' .
                                'Det kan ju bli lite mer påtagligt då; "I eftermiddag så ska de cykla den sträckan jag donerat för".';
            $userMailAsID     = 'Mail adress - ditt användarnamn:';
            $usersName        = 'Ditt namn';
            $userRoleLabel    = 'Är du donator eller medlem i Team Rynkeby?';
            $pickAteamMemberD = 'Välj en medlem i teamet som du vill skänka pengar till';
            $pickAteamMemberT = 'Vem i teamet är du?';
            $submitButtonText = 'Nästa sida';
            break;

        case 'bookings':
            // Common string variables for all pages
            $pageTitle = "Boka mina mil";
            // $userRole has to be defined prior to running this script
            if ($userRole == $roleDonor) {
                $topHeader = 'Välj sträckor som du vill stödja';
            }
            elseif (in_array($userRole, array($roleRider, $roleService))) {
                $topHeader = 'Dina sträckor. Bekräfta/avvisa bokningar';
            }

            // Page specific strings
            $donateTo              = 'Donera till ';
            $legend                = 'Färgförklaring';
            $donorButtonFreeText   = 'Ledig';
            $donorButtonBookedText = 'Bokad';
            $goToCheckout          = 'Donera';
            $free                  = 'Obokad';
            /*** Preliminary/confirm bookings not implemented. Assume all bookings to be true and trust that everyone pays */
            // $bookedByYouPrel       = 'Preliminärbokad av dig';
            // $bookedByOtherPrel     = 'Preliminärbokad av någon annan';
            $bookedByYou           = 'Bokad av dig';
            $bookedByOther         = 'Bokad av någon annan';
            break;

        case 'checkout':
            // Common string variables for all pages
            $pageTitle = 'Godkänn donation';
            $topHeader = 'Godkänn din donation';

            // Page specific strings
            $confirmAndPay   = 'Bekräfta dina val och gå vidare till betalning på MyCollection';
            $payingInfo      = 'OBS! Man kan bara betala med kort på MyCollection';
            $dateText        = 'Datum';
            $legText         = 'Sträcka';
            $youHaveSelected = 'Du har valt att stödja dessa delsträckor för'; // Name of team member will added on web page
            $amountToDonate  = 'Du vill donera'; // Amount will be added on web page
            break;

        case 'saveToDbAndShowMyCollection':
            $pageTitle       = 'Till MyCollection';
            $topHeader       = 'Bekräftelse';
            $redirectMessage = 'Din donation registreras och du skickas nu vidare till MyCollection.<br>';
            $backupLink      = 'Om du inte automatiskt skickas vidare inom några sekunder, klicka på denna rad så kommer du vidare.';
              //                 'Donationen är giltig när mottagaren har bekräftat.';
            $you             = 'Tack! Du';
            $prelBooking     = ' har sponsrat en del av vägen till Paris';
            $for             = ' för ';
            //$bookingText     = ' har bokat nedanstående sträcka/sträckor. Var vänlig att bekräfta när du ser att pengarna kommit in till din MyCollection! OBS! Bekräftningsfunktionen fungerar inte ännu.';
            $thanks          = "\nStort tack för din generösa gåva\n\n%s och Team Rynkeby - God Morgon!";
            break;

        default:
            echo "ERROR: PHP file '$scriptName' does not have any string definitions!";
            return 1;
    }

    // Strings to be used in more than one script
    $distanceText      = 'km';
    $roleDonorTxt      = 'Donator';
    $roleRiderTxt      = 'Cyklist';
    $roleRidersTxt     = 'Cyklister';
    $roleServiceTxt    = 'Serviceteam';
    $roleTeamMemberTxt = 'Teammedlem';
    $notFastEnough     = 'Ojdå! Någon har hunnit före dig att boka någon av de sträckor du valt. Felhanteringen är tyvärr inte så bra här. ' .
                         'Klicka på cykeln för att komma tillbaks till startsidan där du får börja om från början.'

?>
