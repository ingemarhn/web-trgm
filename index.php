<?php

header('Content-type: text/html; charset=UTF-8');
session_start();

require_once "variables.php";
// ToDo: Introduce possibility to select interface language
if (!isset($_SESSION['lang'])) {
    $_SESSION['lang'] = $defaultLang;
}
$lang = $_SESSION['lang'];
$currentUser = isset($_SESSION['teamMemberName']) ? $_SESSION['teamMemberName'] : 'Not Set';
echo '<!DOCTYPE html>' . PHP_EOL;
echo "<html lang='$lang' >" . PHP_EOL;
echo '<head>';

error_reporting(E_ALL);

# Name of this script
$scriptName = basename($_SERVER['SCRIPT_NAME'], '.php');

// Set string variables based on selected language
require_once "strings_$lang.php";
require_once "readData.php";
require_once "stdhead.php";

// Open database and read all donors.
require_once 'dbTools.php';
$db = dbOpen($dbName);
$donors = $db->getAllDonors();
echo '<script>' . PHP_EOL;
echo 'var donors = [];' . PHP_EOL;
while ($donor = $donors->fetchArray()) {
    $mailLower = strtolower($donor['mail_address']);
    echo "donors.push([\"${donor['id']}\", \"${donor['name']}\", \"$mailLower\"]);" . PHP_EOL;
}
echo '</script>' . PHP_EOL;

?>

    <script>
        // This function looks up a mail_address in a matrix defined in the PHP script above
        function indexOf(matrix, mailAddress) {
            var i = matrix.length;
            while (i--) {
                var donor = matrix[i];
                if (donor[2] === mailAddress) {
                    return i;
                }
            }
            return -1;
        }

        /**
        * This script prevents selecting role as Team Member since that is not supported yet.
        * When the functionality is ready, the script should instead be used to modify the form according to which role that is selected.
        */
        $(document).ready(function() {
            /**
            * This script prevents selecting role as Team Member since that is not supported yet.
            * When the functionality is ready, the script should instead be used to modify the form according to which role that is selected.
            */
            $("#identify input:radio").click(function() {
                if (!$("input:radio:first").prop("checked")) {
                    alert('Funktionen inte implementerad än. Du kan bara välja Donator.');
                    $("input:radio:first").prop("checked", true);
                }
            });

            // Insert name in name field if mail address is known
            $("#inpMail").change(function() {
                var nameField = $("#inpName");
                var name = nameField[0].value;
                var mailField = $("#inpMail");
                var mail = mailField[0].value.toLowerCase();

                // If mailaddress is known, insert name in the name input field and set it read-only. Also set hidden id.
                var ind = indexOf(donors, mail);
                if (ind >= 0) {
                    nameField[0].value = donors[ind][1];
                    nameField[0].readOnly = true;
                }
                else {
                    nameField[0].value = '';
                    nameField.select();
                }
            });
        });
    </script>

</head>
<body>
    <div class="container">
        <?php

        require_once 'topHeader.php';

        // Are user values set in this session?
        $userMail     = isset($_SESSION['userMailAddress']) ? $_SESSION['userMailAddress'] : '';
        $userRealName = isset($_SESSION['userRealName'])    ? $_SESSION['userRealName']    : '';

        echo <<<EOT
        <div class="mainArea">
            $initText<img class="zipperSmiley" src="images/zipperSmiley-25.gif" alt="Happy!"><br>
            $projectText <a href="https://www.team-rynkeby.se/team-rynkeby-4.aspx" target="_blank"><img id="logo" src="images/tr-logo-header-godmorgon.png" alt="TRGM logo"></a><br>
            <p>
            $collectionText

            <form id="identify" action="bookings.php" method="POST">
                <label>
                    $userRoleLabel
                    <br>
                </label>
                <input type="radio" name="userRole" value=$roleDonor      checked="checked">$roleDonorTxt<br>
                <input type="radio" name="userRole" value=$roleTeamMember                  >$roleTeamMemberTxt<br>
                <p>
                <div id="usrMail">
                    <label for="inpMail" id="LusrMail">
                        $userMailAsID
                        <br>
                    </label>
                    <input type="text" id="inpMail" name="userMailAddress" value="$userMail"     required>
                </div>
                <div id="usrName">
                    <label for="inpName" id="LusrName">
                        $usersName
                        <br>
                    </label>
                    <input type="text" id="inpName" name="userRealName"    value="$userRealName" required>
                </div>

                <p>
                $pickAteamMemberD<br>
                <select name="member" required>
                    <option value=''>&nbsp;</option>
                    <option disabled>--- $roleRidersTxt ---</option>
EOT;
                    foreach ($teamMembers[$roleRider] as $ind => $member) {
                        $optValue = implode('|', $member);
                        echo "<option value='$optValue'" . ($currentUser == $member[0] ? ' selected' : '') .">" . $member[0] . "</option>";
                    }
                    echo "<option disabled>--- $roleServiceTxt ---</option>";
                    foreach ($teamMembers[$roleService] as $ind => $member) {
                        $optValue = implode('|', $member);
                        echo "<option value='$optValue'" . ($currentUser == $member[0] ? ' selected' : '') .">" . $member[0] . "</option>";
                    }

            ?>
                </select>

                <input type="submit" value=<?php echo "'$submitButtonText'"; ?> id="indxNextPg">
            </form>
        </div>
    </div>
</body>
</html>
