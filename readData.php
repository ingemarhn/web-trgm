<?php

    require_once("variables.php");
    // Open the database
    require_once('dbTools.php');
    $db = dbOpen($dbName);

    # This file reads all data files and stores the content in variables

    # TO BE READ FROM DATA FILE
    $distances = [
        '2019-06-29' => ['<span class="prologue">Prolog</span>: Löddeköpinge -<br>Trelleborg',  66],
        '2019-06-30' => ['Travemünde -<br>Verden', 200],
        '2019-07-01' => ['Verden -<br>Münster', 200],
        '2019-07-02' => ['Münster -<br>Roermond', 180],
        '2019-07-03' => ['Roermond -<br>Namur', 150],
        '2019-07-04' => ['Namur -<br>Charleville-Mezieres', 150],
        '2019-07-05' => ['Charleville-MezieresF -<br>Château-Thierry', 150],
        '2019-07-06' => ['Château-Thierry -<br>Paris', 110],
    ];

    // Team members
    // ToDo: KOLLA DATUM PÅ FILEN OCH I DATABASENS ADMIN FÄLT
    // Nyare fil: Läs filen och lägg till eller uppdatera medlemmarna.
    $fh = fopen('teamMembers.txt', 'r');
    while ($line = fgets($fh)) {
        $line = trim(preg_replace('/#.*/', '', $line));
        switch ($line) {
            case '@Riders:':
                $role = $roleRider;
                $i = 0;
                break;

            case '@Service:':
                $role = $roleService;
                $i = 0;
                break;

            case '':
                break;

            default:
                list($name, $email, $myCollection) = explode('|', $line);
                $name  = trim($name);
                $email = trim($email);
                $teamMembers[$role][$i++] = array($name, $email, $myCollection);
                $id = md5($name . $email);
                $db->insertTeamMember($id, $name, $email, '', $role, $myCollection, $defaultFee);
        }
    }
    fclose($fh);

?>
